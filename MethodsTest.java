    public class MethodsTest{
	
	public static void main(String[] args){
		
		int x = 5;
		System.out.println(x);
		methodNoInputReturn();
		System.out.println(x);
		
	    System.out.println(x);
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		
		methodTwoInputNoReturn(8,7.5);
		
	
		int a = methodNoInputReturnInt();
		System.out.println(a);
		
		double b=sumSquareRoot(9,5);
		System.out.println(b);
		
		String s1="java";
		String s2="programming";
		
		System.out.println(s1.length() );
		System.out.println(s2.length() );
		
		System.out.println(SecondClass.addOne(50) );
		
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
		
		
	}
	
	public static void methodNoInputReturn(){
		
		System.out.println("I'm in a method that takes no input and returns nothing");
	int x = 20;
	
	System.out.println(x);
	
	}
	
	public static void methodOneInputNoReturn(int input){
		
		input=input-5;
		System.out.println("Inside the method one input no return");
		System.out.println(input);
	}

	public static void methodTwoInputNoReturn(int num1,double num2){
		    
    
	System.out.println(num1);
	System.out.println(num2);
	}
	
	public static int methodNoInputReturnInt(){
		int num=5;
		return num;
	}
	
	public static double sumSquareRoot(int num1,int num2){
		int sum = num1+num2;
		double squareRoot=Math.sqrt(sum);
		
		return squareRoot;
		
	}
	
	
	
		
	
		
}