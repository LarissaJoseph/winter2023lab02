public class Calculator{
	
	public static int addMethod(int num1,int num2){
		int sum = num1 + num2;
		
		return sum;
	}
	
	public static int subMethod(int num1,int num2){
		
		int difference = num1 - num2;
		
		return difference;
	}
	
	public int multMethod(int num1,int num2){
		
		int multiplied = num1*num2;
		
		return multiplied;
	}
	
	public int divMethod(int num1,int num2){
		
		int divided = num1/num2;
		
		return divided;
	} 
}